<?php
// Drupal likes the php module files to not have a closing tag

/*
 * A quick helper function for revealing contents and structure
 */

function mike_explain($element = array(), $depth = 1, $lead = '') {
  $string = '';
  $cap = ' <br>';  // might be <br> or \n or a unique character for serialization
  if ($depth < 1) {
    return '';
  }
  if (is_array($element)) {
    foreach ($element as $key => $part) {
      $string .= ($depth == 1) ? '[' . $key . ']' : mike_explain($part, $depth - 1, $lead . '[' . $key . ']');
    }
  }
  elseif (is_object($element)) {
    foreach ($element as $key => $part) {
      $string .= ($depth == 1) ? '->' . $key : mike_explain($part, $depth - 1, $lead . '->' . $key);
    }
  }
  else {
    $string .= $lead . ' = ' . $element . $cap;
  }
  return $string;
}

/*
 * helper function for removing html from report contents
 * Strip only selcted tags in THEME_preprocess_node function below
 * strip defined constant tags if none given
 */

function module_strip_only($str, $tags = NULL) {
  if ($tags == NULL) {
    $tags = explode('/', UNWANTED_REPORT_TAGS);
  }
  if (!is_array($tags)) {
    $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
    if (end($tags) == '') {
      array_pop($tags);
    }
  }
  foreach ($tags as $tag) {
    $str = preg_replace('#</?' . $tag . '[^>]*>#is', '', $str);
  }
  return $str;
}

